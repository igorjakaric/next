<?php
/**
 * The template for displaying posts pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nex
 */

get_header(); ?>

  <main id="main" class="site-main" <?php echo $main_microdat=( is_front_page() || is_home()) ?
    'itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainEntityOfPage"' : ((is_page()) ? 'itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainContentOfPage"' : ''); ?>>
    <div class="container">
      <?php
        if ( have_posts() ) : ?>
        <?php
            while ( have_posts() ) : the_post(); ?>
          <article id="blog-post-<?php the_ID(); ?>" class="hentry article-excerpt" itemscope itemtype="http://schema.org/Article">
            <figure class="article-image" style="background-image:url('<?php echo get_the_post_thumbnail_url( $post->ID, 'medium' ); ?>')">
              <meta itemprop="image" content="<?php echo get_the_post_thumbnail_url( $post->ID, 'thumbnail' ); ?>">
            </figure>
            <h3 class="entry-title article-title" itemprop="name headline">
              <a href="<?php echo the_permalink(); ?>" rel="bookmark" itemprop="url">
                <?php echo the_title(); ?>
              </a>
            </h3>
            <?php echo the_excerpt(); ?>
            <span class="article-read-more read-more-btn">
              <a href="<?php echo the_permalink(); ?>">Read more</a>
            </span>
            <br/>
            <time class="published article-date">
              <?php echo get_the_date(); ?>
            </time>
            <br/>
            <time class="modified article-updated">
              <?php echo the_modified_date(); ?>
            </time>

            <!-- OVAJ DEO ISPOD NE BRISATI -->
            <span style="display:none;">
              <time itemprop="datePublished">
                <?php echo get_the_date('c'); ?>
              </time>
              <time itemprop="dateModified">
                <?php echo the_modified_date('c'); ?>
              </time>
            </span>
            <span style="display:none;" itemprop="author" itemscope itemtype="https://schema.org/Person">
              <meta itemprop="name" content="<?php echo get_author_name(); ?>">
            </span>
            <span style="display:none;" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
              <span itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                <meta itemprop="url" content="http://www.example.com/slika.jpg">
                <meta itemprop="width" content="600">
                <meta itemprop="height" content="200">
              </span>
              <meta itemprop="name" content="Ime organizacije">
              <link itemprop="url" href="/">
            </span>
            <!-- OVAJ DEO IZNAD NE BRISATI -->

          </article>
          <?php endwhile; ?>

          <div class="blog-pagination">
            <?php custom_paging_nav(); ?>
          </div>

          <?php else :
          get_template_part( 'template-parts/content', 'none' );
        endif; ?>
    </div>
  </main>
  <!-- #main -->

  <?php
get_sidebar();
get_footer();