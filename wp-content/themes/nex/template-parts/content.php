<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nex
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">
	<div class="entry-header">
		<?php the_title( '<h1 class="entry-title" itemprop="name headline">', '</h1>' ); ?>
		
		<span style="display:none;"><meta itemprop="image" content="<?php echo get_the_post_thumbnail_url(); ?>"></span>
		
		<?php if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php nex_posted_on(); ?>
			</div><!-- .entry-meta -->
		<?php endif; ?>
	</div><!-- .entry-header -->

	<div class="entry-content" itemprop="articleBody">
		<?php the_content(); ?>
	</div><!-- .entry-content -->
	<span style="display:none;">
		<time itemprop="datePublished"><?php echo get_the_date('c'); ?></time>
		<time itemprop="dateModified"><?php echo the_modified_date('c'); ?></time>
	</span>
	<span style="display:none;" itemprop="author" itemscope itemtype="https://schema.org/Person">
		<meta itemprop="name" content="<?php echo get_author_name(); ?>">
	</span>
	<span style="display:none;" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
		<span itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
			<meta itemprop="url" content="http://www.example.com/slika.jpg">
			<meta itemprop="width" content="600">
			<meta itemprop="height" content="200">
		</span>
		<meta itemprop="name" content="Ime organizacije">
		<link itemprop="url" href="/">
	</span>
</article><!-- #post-<?php the_ID(); ?> -->
