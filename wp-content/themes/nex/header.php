<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nex
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.ico"/>
	<?php wp_head(); ?>
</head>

<?php
	$contact_id = array(300, 500, 400);
	$services_id = array(111, 222, 333);
	$about_id = array(123, 321, 543);
?>

<body <?php body_class(); ?> <?php echo $body_microdat = (is_front_page()) ? 'itemscope itemtype="http://schema.org/WebSite"' : ((is_home()) ? 'itemscope itemtype="http://schema.org/Blog"' : ((is_page($services_id)) ? 'itemscope itemtype="http://schema.org/Service"' : ((is_page($about_id)) ? 'itemscope itemtype="http://schema.org/AboutPage"' : ((is_page($contact_id)) ? 'itemscope itemtype="http://schema.org/ContactPage"' : ((is_page()) ? 'itemscope itemtype="http://schema.org/WebPage"' : ''))))); ?>>


<header class="site-header">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'nex' ); ?></a>
	<div class="container-wrapper">
		<div class="header-top flex line-after relative">
			<a id="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
				<img src="<?php echo get_template_directory_uri();?>/images/header-logo.png">
			</a>
			<?php $link = get_field('header_button'); if( $link ): ?>
				<a class="button button--green button--underline lspacing" href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a>
			<?php endif; ?>
		</div>
	</div>
	<nav id="site-navigation" class="main-navigation" data-main-nav>
		<div class="container-wrapper">
			<div class="menu-btn" data-menu-btn><span></span></div>    <!-- menu-button -->
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
				'menu_class'     => 'primary-menu clear',
				'container'      => false
			) );
			?>

			<div class="button-holder">
				<?php $link = get_field('header_button'); if( $link ): ?>
					<a class="button button--green button--underline lspacing" href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a>
				<?php endif; ?>
			</div>
		</div>
	</nav><!-- #site-navigation -->
</header>