<?php
/**
 * nex functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package nex
 */

require_once 'inc/pretty_dump.php';

if ( ! function_exists( 'nex_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function nex_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on nex, use a find and replace
		 * to change 'nex' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'nex', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 add image size
		 */

		add_image_size('hotel-thumbnail', 258, 154, array( 'center', 'center' ));
		add_image_size('keynote-image', 386, 243, array( 'center', 'center' ));
		add_image_size('custom-post-type-thumbnail', 330, 227, array( 'center', 'center' ));
		add_image_size('custom-post-type-small', 121, 121, array( 'center', 'center' ));

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'nex' ),
			'menu-2' => esc_html__( 'Secondary', 'nex' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'nex_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'nex_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function nex_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'nex_content_width', 640 );
}

add_action( 'after_setup_theme', 'nex_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function nex_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'nex' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'nex' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}

add_action( 'widgets_init', 'nex_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function nex_scripts() {

	wp_enqueue_style( 'nex-plugins-css', get_template_directory_uri() . '/dist/plugins.min.css' );

	wp_enqueue_style( 'nex-style', get_stylesheet_uri() );

	wp_enqueue_script( 'jquery' );

	wp_enqueue_script( 'nex-plugins-js', get_template_directory_uri() . '/dist/plugins.min.js', array(), '', true );

	wp_enqueue_script( 'nex-site-js', get_template_directory_uri() . '/dist/site.min.js', array(), '', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'nex_scripts' );

/**
 * Add a special class to the excerpt's p element
 */
add_filter( "the_excerpt", "add_class_to_excerpt" );

function add_class_to_excerpt( $excerpt ) {
    return str_replace('<p', '<p class="entry-summary article-excerpt" itemprop="description"', $excerpt);
}

/**
 * Show post and page ID in dashboard
 */

function revealid_add_id_column( $columns ) {
   $columns['revealid_id'] = 'ID';
   return $columns;
}
function revealid_id_column_content( $column, $id ) {
  if( 'revealid_id' == $column ) {
    echo $id;
  }
}

add_filter( 'manage_posts_columns', 'revealid_add_id_column', 5 );
add_action( 'manage_posts_custom_column', 'revealid_id_column_content', 5, 2 );
add_filter( 'manage_pages_columns', 'revealid_add_id_column', 5 );
add_action( 'manage_pages_custom_column', 'revealid_id_column_content', 5, 2 );
add_filter( 'manage_media_columns', 'revealid_add_id_column', 5 );
add_action( 'manage_media_custom_column', 'revealid_id_column_content', 5, 2 );
add_filter( 'manage_project_columns', 'revealid_add_id_column', 5 );
add_action( 'manage_project_custom_column', 'revealid_id_column_content', 5, 2 );


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * Add options page
 */

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();

	$cookies = array(
		'page_title' => 'Cookies',
		'post_id'    => 'cookies',
		'icon_url'   => 'dashicons-businessman',
		'position'   => '26.9'
	);

	acf_add_options_page( $cookies );
}

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );