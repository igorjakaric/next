<?php
/**
 * Template Name: Homepage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nex
 */

get_header(); ?>

<main id="main" class="site-main" <?php echo $main_microdat=( is_front_page() || is_home()) ?
 'itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainEntityOfPage"' : ((is_page()) ?
 'itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainContentOfPage"' : '' ); ?>>
	<!-- Content : Start -->
	<div id="content" class="content-wrapper">
		<section id="section-1" class="section-1 two-columns flex" itemprop="text">
			<div class="col-md-6 col-left bg-purple">
				<div class="col-left__content txt-white">
					<span class="subtitle" data-aos="fade" data-aos-easing="ease" data-aos-duration="1000"><?php the_field('summit_date');?></span>
					<h2>
						<span id="typed-strings">
							<span>
								<?php the_field('summit_title', false, false); ?>
							</span>
						</span>
						<span id="typed" class="typed"></span>
					</h2>
					<p data-aos="fade-up" data-aos-easing="ease" data-aos-delay="2100"
						    data-aos-duration="1000" data-aos-once="true"><?php the_field('summit_text'); ?></p>
					<button class="button button--transparent button--underline modal-trigger" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="2250"
						    data-aos-duration="1000" data-aos-once="true"><?php the_field('summit_btn'); ?></button>
				</div>

				<div class="modal modal--form">
					<div class="modal__content">
						<span class="modal__close">x</span>
						<h3 class="modal__title">Full Programme Request</h3>
						<p class="modal__text">Fill your details in the form and we will send you the whole brochure filled with all details</p>
						<?php echo do_shortcode('[contact-form-7 id="113" title="Program Request"]');?>
						<p class="modal__text--bottom">We will not share your details, check out our <a href="#">Terms and conditions.</a></p>
					</div>
				</div>
			</div>

			<div class="col-md-6" style="background-image: url(<?php the_field('summit_image'); ?>);">
				<!--  -->
			</div>
		</section>
		<!-- Section 2: Start -->
		<section id="section-2" class="section-2 two-columns flex">
			<div class="col-md-6 background" style="background-image: url(<?php the_field('accommodation_background'); ?>"></div>
			<div class="col-md-6 col-left">
				<div class="col-left__content">
					<span class="subtitle" data-aos="fade" data-aos-easing="ease" data-aos-duration="1000"><?php the_field('accommodation_date');?></span>
					<h2>
						<span id="typed-strings2">
							<span>
								<?php the_field('accommodation_title', false, false); ?>
							</span>
						</span>
						<span id="typed2" class="typed"></span>
					</h2>
					<p data-aos="fade-up" data-aos-easing="ease" data-aos-delay="2100"
						    data-aos-duration="1000" data-aos-once="true"><?php the_field('accommodation_text'); ?></p>

					<?php $accommodation_link_1 = get_field('accommodation_link_1'); ?>
					<?php $accommodation_link_2 = get_field('accommodation_link_2'); ?>

					<a class="button button--green button--underline" href="<?php echo $accommodation_link_1['url']; ?>" target="<?php echo $accommodation_link_1['target']; ?>" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="2250"
						    data-aos-duration="1000" data-aos-once="true">
						<?php echo $accommodation_link_1['title']; ?>
					</a>

					<a class="button button--green-border modal-trigger" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="2250"
						    data-aos-duration="1000" data-aos-once="true">
						<?php echo $accommodation_link_2['title']; ?>
					</a>

				</div>

				<div class="modal modal--grid">
					<div class="modal__content">
						<span class="modal__close">x</span>
						<h3 class="modal__title">Book a Hotel</h3>
						<div class="modal__grid">
							<?php if( have_rows('hotels_wrapper') ): ?>
								<?php while ( have_rows('hotels_wrapper') ) : the_row(); ?>
									<div class="modal__item">
										<div>
											<img src="<?php the_sub_field('hotel_image');?>" alt="">
										</div>
										<div>
											<h5 class="modal__item__title"><?php the_sub_field('hotel_name');?></h5>
											<a href="<?php the_sub_field('hotel_url');?>" class="button button--purple-border" target="_blank">Show prices</a>
										</div>
									</div>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Section 2: End -->

		<!-- Section 3: Start -->
		<div class="section-3">
			<div class="container-wrapper">
				<section class="counter">
					<div class="flex">
						<?php if( have_rows('counter_wrapper') ): ?>
						<?php $i =1; ?>
						<?php while ( have_rows('counter_wrapper') ) : the_row(); ?>
								<div class="counter__item">
									<h4 class="counter__item__title">
										<?php the_sub_field('counter_title'); ?>
									</h4>
									<span id="countUp<?php echo $i; ?>" class="counter__item__number color-purple">
										<?php //the_sub_field('counter_number'); ?>
										0
									</span>

								</div>
								<?php $i++; ?>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</section>
			</div>
		</div>
		<!-- Section 3: End -->

		<!-- Section 4: Start -->
		<section class="section-4">
			<div class="flex container-wrapper padding-t">
				<?php if( have_rows('three_column_wrapper') ): ?>
					<?php while ( have_rows('three_column_wrapper') ) : the_row(); ?>
						<div class="three-column-wrapper">
							<div class="three-column-wrapper__image border">
								<div class="three-column-wrapper__background-wrapper" style="background-image: url('<?php the_sub_field('three_column_background'); ?>')">
								</div>
							</div>
							<div class="three-column-wrapper__content">
								<h3 class="three-column-wrapper__title">
									<?php the_sub_field('three_column_title'); ?>
								</h3>
								<p class="three-column-wrapper__txt">
									<?php the_sub_field('three_column_txt'); ?>
								</p>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</section>
		<!-- Section 4: End -->

		<!-- Section 5: Start -->
		<section id="section-5" class="section-5 section-wrapper grey-light line-after line-before">
			<h2 class="section-title line-after line-after__small line-after__small__purple"><?php the_field('four_box_title'); ?></h2>
			<p class="section-title__txt"><?php the_field('four_box_title_txt'); ?></p>
			<div class="container-wrapper">
				<div class="four-box-grid">
					<?php if( have_rows('four_box_section') ): ?>
						<?php while ( have_rows('four_box_section') ) : the_row(); ?>
							<div class="four-box">
								<div class="four-box__content four-box__trigger">
									<div class="four-box__image">
										<img src="<?php the_sub_field('four_box_image');?>">
									</div>
										<h5 class="four-box__title">
											<?php the_sub_field('four_box_title');?>
										</h5>
										<span class="four-box__subtitle">
											<?php the_sub_field('four_box_content');?>
										</span>
									</div>
									<div class="four-box__description">
										<p class="four-box__text">
											<span class="four-box__text-title">ABOUT</span>
											<?php the_sub_field('four_box_description');?>
										</p>
									</div>

									<?php if( have_rows('speaker_socials') ): ?>
										<div class="four-box__socials">
											<?php while( have_rows('speaker_socials') ): the_row();?>

												<?php

													$icon = get_sub_field('icon');
													$link = get_sub_field('link');

												?>

												<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><span class="font-<?php echo $icon ?>"></span></a>
											<?php endwhile; ?>
										</div>
									<?php endif; ?>

							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="speakers__button-wrapper">
				<a href="javascript;:" class="speakers__button color-purple relative">
					<span>See all Speakers&nbsp;</span>
					<span class="font-right-arrow"></span>
				</a>
			</div>
		</section>
		<!-- Section 5: End -->

		<!-- Section 6 : Start -->
		<section id="section-6" class="section-6 container-wrapper">
			<h2 class="section-title line-after line-after__small line-after__small__green">
				<?php the_field('agenda_title'); ?>
			</h2>
			<p class="section-title__txt">
				<?php the_field('agenda_title_txt'); ?>
			</p>
			<!-- Tabs Nav : Start -->

			<ul class="tabs tab-list tab-list--big flex nav nav-tabs" role="tablist">
				<li class="tab-list__item tab-list--big__item txt-white" role="presentation">
					<a href="#tab1" aria-controls="profile" role="tab" data-toggle="tab">Wednesday, September 18</a>
				</li>
				<li class="tab-list__item tab-list--big__item txt-white" role="presentation">
					<a data-target="#tab2" aria-controls="profile" role="tab" data-toggle="tab">Thursday, September 19</a>
				</li>
			</ul>
			<!-- Tabs Nav : End -->


			<!-- Tabs Content : Start -->
			<div class="tab-content">

				<!-- Tabs 1 : Start -->
				<div id="tab1" class="tab-pane active tab-content" role="tabpanel">
					<ul class="tabs tab-list tab-list--small nav nav-tabs" role="tablist">
						<li class="tab-list__item tab-list--small__item txt-white" role="presentation">
							<a href="#tab2-1" aria-controls="profile" role="tab" data-toggle="tab">Track 1</a>
						</li>
						<li class="tab-list__item tab-list--small__item txt-white" role="presentation">
							<a data-target="#tab2-2" aria-controls="profile" role="tab" data-toggle="tab">Track 2</a>
						</li>
					</ul>
					<div id="tab2-1" class="tab-pane active tab-content" role="tabpanel">
						<?php if( have_rows('day_one_before_noon') ): ?>
							<?php while ( have_rows('day_one_before_noon') ) : the_row(); ?>
								<div class="flex">
									<div class="flex__time">
										<a href=""><?php the_sub_field('day_one_time_before');?></a>
									</div>
									<div class="flex__content">
										<a href=""><h5><?php the_sub_field('day_one_wrapper_title_before');?></h5></a>
										<!-- <p><?php the_sub_field('day_one_wrapper_content_before');?></p>	 -->
										<!-- <img src="<?php the_sub_field('day_one_thumbnail_before');?>" alt=""> -->
										<div class="flex">
											<h6><?php the_sub_field('day_one_speaker_name_before');?></h6>
											<p><?php the_sub_field('day_one_wrapper_interest_before');?></p>
										</div>
									</div>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
					<div id="tab2-2" class="tab-pane tab_content" role="tabpanel">
						<?php if( have_rows('day_one_wrapper_after_noon') ): ?>
							<?php while ( have_rows('day_one_wrapper_after_noon') ) : the_row(); ?>
								<div class="flex">
									<div class="flex__time">
										<a href=""><?php the_sub_field('day_one_time_after');?></a>
									</div>
									<div class="flex__content">
										<a href=""><h5><?php the_sub_field('day_one_wrapper_title_after');?></h5></a>
										<!-- <p><?php the_sub_field('day_one_wrapper_content_after');?></p>	 -->
										<!-- <img src="<?php the_sub_field('day_one_thumbnail_after');?>" alt=""> -->
										<div class="flex">
											<h6><?php the_sub_field('day_one_speaker_name_after');?></h6>
											<p><?php the_sub_field('day_one_wrapper_interest_after');?></p>
										</div>
									</div>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
				<!-- Tab 1 : End -->

				<!-- Tab 2 : Start -->
				<div id="tab2" class="tab-pane tab_content" role="tabpanel">
					<ul class="tabs tab-list tab-list--small">
						<li class="tab-list__item tab-list--small__item"><a href="#tab4-1">Track 1</a></li>
						<li class="tab-list__item tab-list--small__item"><a href="#tab4-2">Track 2</a></li>
					</ul>
					<div id="tab4-1" class="tab-pane active tab_content" role="tabpanel">
						<?php if( have_rows('day_two_before_noon') ): ?>
							<?php while ( have_rows('day_two_before_noon') ) : the_row(); ?>
								<div class="flex">
									<div class="flex__time">
										<a href=""><?php the_sub_field('day_two_time_before');?></a>
									</div>
									<div class="flex__content">
										<a href=""><h5><?php the_sub_field('day_two_wrapper_title_before');?></h5></a>
										<!-- <p><?php the_sub_field('day_two_wrapper_content_before');?></p>	 -->
										<!-- <img src="<?php the_sub_field('day_two_thumbnail_before');?>" alt=""> -->
										<div class="flex">
											<h6><?php the_sub_field('day_two_speaker_name_before');?></h6>
											<p><?php the_sub_field('day_two_wrapper_interest_before');?></p>
										</div>
									</div>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
					<div id="tab4-2" class="tab-pane tab_content" role="tabpanel">
						<?php if( have_rows('day_two_wrapper_after_noon') ): ?>
							<?php while ( have_rows('day_two_wrapper_after_noon') ) : the_row(); ?>
								<div class="flex">
									<div class="flex__time">
										<a href=""><?php the_sub_field('day_two_time_after');?></a>
									</div>
									<div class="flex__content">
										<a href=""><h5><?php the_sub_field('day_two_wrapper_title_after');?></h5></a>
										<!-- <p><?php the_sub_field('day_two_wrapper_content_after');?></p>	 -->
										<!-- <img src="<?php the_sub_field('day_two_thumbnail_after');?>" alt=""> -->
										<div class="flex">
											<h6><?php the_sub_field('day_two_speaker_name_after');?></h6>
											<p><?php the_sub_field('day_two_wrapper_interest_after');?></p>
										</div>
									</div>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
				<!-- Tabs 2 : End -->

			</div>
			<!-- Tab Content : End -->
		</section>
		<!-- Section 6 : End -->

		<!-- Section 7 : Start -->
		<section id="section-7" class="section-7 silver grey-light line-after line-before">
			<div class="container-wrapper">
				<h2 class="section-title price-title"><?php the_field('price_title') ;?></h2>
				<p class="section-title__txt"><?php the_field('price_title_txt'); ?></p>


				  <div class="card">

					<div class="front two-column-small flex">
						<?php if( have_rows('price_wrapper') ): ?>
							<?php while ( have_rows('price_wrapper') ) : the_row(); ?>
								<div class="two-column-small__content border white">
									<h4 class="two-column-small__title"><?php the_sub_field('price_wrapper_title');?></h4>
									<div class="flex">
									<?php if( get_sub_field('price_wrapper_old_price') ): ?>
										<div class="two-column-small__price two-column-small__price--small flex green align-center">
											<span class="font-eur"></span>
											<span class="cross"><?php the_sub_field('price_wrapper_old_price');?></span>
										</div>
									<?php endif; ?>
									<?php if( get_sub_field('price_wrapper_new_price') ): ?>
											<div class="two-column-small__price two-column-small__price--big flex green align-center">
												<span class="font-eur"></span>
												<span class="green"><?php the_sub_field('price_wrapper_new_price');?></span>
											</div>
										</div>
									<?php endif; ?>
									<span class="two-column-small__txt"><?php the_sub_field('price_wrapper_information');?></span>
									<ul>
										<?php if( have_rows('price_information_content') ): ?>
											<?php while ( have_rows('price_information_content') ) : the_row(); ?>
												<li class="flex"><span class="font-checkmark"></span><?php the_sub_field('price_information_txt');?></li>
											<?php endwhile; ?>
										<?php endif; ?>
									</ul>
									<button class="button">Book Now</button>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>


					<div class="back">
						<?php echo do_shortcode('[contact-form-7 id="113" title="Program Request"]');?>
					</div>
				</div>
			</div>
		</section>
		<!-- Section 7 : End -->

		<!-- Section 8 : Start -->
		<section id="section-8" class="section-8 container-wrapper">
			<h2 class="section-title line-after line-after__small line-after__small__green"><?php the_field('sponsors_title');?></h2>
			<p class="section-title__txt"><?php the_field('sponsors_title_txt');?></p>
			<div class="four-box-grid align-center">
				<?php if( have_rows('sponsors_wrapper') ): ?>
					<?php while ( have_rows('sponsors_wrapper') ) : the_row(); ?>
						<div class="four-box four-box-section sponsors">
							<a href="<?php the_sub_field('sponsors_wrapper_url');?>">
								<img src="<?php the_sub_field('sponsors_wrapper_image');?>" alt="">
							</a>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>

			<?php
				$sponsor_button = get_field('sponsor_button');
			if( $sponsor_button ): ?>
				<a class="button button--green button--lower button--center" href="mailto:<?php echo $sponsor_button['url']; ?>"><?php echo $sponsor_button['title']; ?></a>
			<?php endif; ?>

		</section>
		<!-- Section 8 : End -->

		<!-- Section 9 : Start -->
		<section id="section-9" class="section-9 bg-purple">
			<div class="container">
				<div class="row content-wrapper flex">
					<h4 class="txt-white"><?php the_field('request_txt');?></h4>
					<button class="button button--green button--underline modal-trigger"><?php the_field('request_btn');?></button>

					<div class="modal modal--form">
						<div class="modal__content">
							<span class="modal__close">x</span>
							<h3 class="modal__title">Full Programme Request</h3>
							<p class="modal__text">Fill your details in the form and we will send you the whole brochure filled with all details</p>
							<?php echo do_shortcode('[contact-form-7 id="113" title="Program Request"]');?>
							<p class="modal__text--bottom">We will not share your details, check out our <a href="#">Terms and conditions.</a></p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Section 9 : End -->
	</div>
	<!-- Content : End -->


	<div class="modal modal--cookie">
		<div class="modal__content">
			<p class="modal__text"><?php the_field('cookie_text', 'cookies') ?></p>
			<div class="link-wrapper">
				<?php $cookie_link1 = get_field('cookie_link_1', 'cookies'); ?>
				<?php $cookie_link2 = get_field('cookie_link_2', 'cookies'); ?>

				<a class="modal--cookie__more" href="<?php echo $cookie_link1['url']; ?>" target="<?php echo $cookie_link1['target']; ?>">
					<?php echo $cookie_link1['title']; ?>
				</a>
				<a class="modal--cookie__close" target="<?php echo $cookie_link2['target']; ?>">
					<?php echo $cookie_link2['title']; ?>
				</a>
			</div>
		</div>
	</div>
	<?php get_sidebar(); ?>

</main>
<?php
get_footer();