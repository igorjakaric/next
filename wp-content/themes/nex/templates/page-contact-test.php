<?php
/**
 * Template Name: Contact Test
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nex
 */

get_header(); ?>

  <main id="main" class="site-main" <?php echo $main_microdat=( is_front_page() || is_home()) ? 'itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainEntityOfPage"' : ((is_page()) ? 'itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainContentOfPage"' : ''); ?>>
    <div id="content" class="content-wrapper">

      <section class="container" itemprop="text">
        <h2>Contact Page</h2>
        <p>Bacon t-bone strip steak, meatloaf picanha tri-tip short ribs ham hock shank rump alcatra pork capicola turkey prosciutto.
          Doner spare ribs ball tip fatback, frankfurter shoulder brisket pork loin capicola sausage meatball rump. Ham hock
          jowl kevin, chicken salami boudin swine pork chop landjaeger. Fatback meatball meatloaf ribeye. Swine pastrami
          ham hock shank t-bone, pig frankfurter tail pork belly kevin turducken drumstick salami brisket sausage.</p>
      </section>

    </div>
    <!-- #content -->

  </main>
  <?php
get_footer();