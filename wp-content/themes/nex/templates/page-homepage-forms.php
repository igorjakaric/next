<?php
/**
 * Template Name: Homepage - forms
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nex
 */

get_header(); ?>

<main id="main" class="site-main" <?php echo $main_microdat=( is_front_page() || is_home()) ?
 'itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainEntityOfPage"' : ((is_page()) ?
 'itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainContentOfPage"' : '' ); ?>>
	<div id="content" class="content-wrapper">
		<section class="two-columns flex line-before" itemprop="text">
			<div class="two-columns__content">
				<div class="purple txt-white relative">
					<div class="entry-content">
						<span><?php the_field('summit_date');?></span>
						<?php the_field('summit_content'); ?>
						<a href="javascript;:" class="border"><?php the_field('summit_btn'); ?></a>
					</div>
					<div class="request-form">
						<?php echo do_shortcode('[contact-form-7 id="113" title="Program Request"]');?>
						<p class="request-form__txt">We will not share your details, check out our <a href="javascript;:">Terms and conditions.</a></p>
					</div>
				</div>
			</div>
			<div class="two-columns__content background" style="background-image: url(<?php the_field('summit_image'); ?>);">
				<!-- <img src="<?php the_field('summit_image'); ?>"> -->
			</div>
		</section>
		<section class="two-columns flex reverse line-after" itemprop="text">
			<div class="two-columns__content">
				<div class="grey-light">
					<div class="entry-content">
						<span class="green"><?php the_field('summit_date');?></span>
						<?php the_field('accommodation_content'); ?>
						<a href="javascript;:"><?php the_field('accommodation_flight'); ?></a>
						<a href="javascript;:"><?php the_field('accommodation_hotel'); ?></a>
					</div>
					<div class="request-form-hotel">
						<div> <!-- Ovde ce ici flex display + ACF repeater!  -->
							<?php if( have_rows('hotels_wrapper') ): ?>
								<?php while ( have_rows('hotels_wrapper') ) : the_row(); ?>
									<div> <!-- Ovde ce ici flex display hotela u 2 kolone!  -->
										<div>
											<img src="<?php the_sub_field('hotel_image');?>" alt="">
										</div>
										<div>
											<span><?php the_sub_field('hotel_name');?></span>
											<a href="<?php the_sub_field('hotel_url');?>">Show prices</a>
										</div>
									</div>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="two-columns__content background" style="background-image: url(<?php the_field('accommodation_image'); ?>">
				<!-- <img src="<?php the_field('accommodation_image'); ?>"> -->
			</div>
		</section>
		<div class="line-after">
			<div class="container-wrapper">
				<section class="counter">
					<div class="flex">
						<?php if( have_rows('counter_wrapper') ): ?>
							<?php while ( have_rows('counter_wrapper') ) : the_row(); ?>
								<div class="counter__item">
									<h4 class="counter__item__title">
										<?php the_sub_field('counter_title'); ?>
									</h4>
									<span class="counter__item__number color-purple">
										<?php the_sub_field('counter_number'); ?>
									</span>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</section>
			</div>
		</div>
		<div class="container-wrapper padding-t">
			<div class="flex">
				<?php if( have_rows('three_column_wrapper') ): ?>
					<?php while ( have_rows('three_column_wrapper') ) : the_row(); ?>
						<section class="three-column-wrapper">
							<div class="three-column-wrapper__image border">
								<div class="three-column-wrapper__background-wrapper" style="background-image: url('<?php the_sub_field('three_column_background'); ?>')">
								</div>
							</div>
							<div class="three-column-wrapper__content">
								<h3 class="three-column-wrapper__title">
									<?php the_sub_field('three_column_title'); ?>
								</h3>
								<p class="three-column-wrapper__txt">
									<?php the_sub_field('three_column_txt'); ?>
								</p>
							</div>
						</section>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
		<div class="section-wrapper grey-light line-after line-before padding-t">
			<h2 class="section-title line-after line-after__small line-after__small__purple"><?php the_field('four_box_title'); ?></h2>
			<p class="section-title__txt"><?php the_field('four_box_title_txt'); ?></p>
			<div class="container-wrapper">
				<div class="flex wrap">
					<?php if( have_rows('four_box_section') ): ?>
						<?php while ( have_rows('four_box_section') ) : the_row(); ?>
							<section class="four-box">
								<div class="four-box__image">
									<img src="<?php the_sub_field('four_box_image');?>">
								</div>
								<div class="four-box__wrapper">
									<h5 class="four-box__title">
										<?php the_sub_field('four_box_title');?>
									</h5>
									<span class="four-box__content">
										<?php the_sub_field('four_box_content');?>
									</span>
									<p class="four-box__description">ABOUT<?php the_sub_field('four_box_description');?></p>
									<div class="line-before">
										<a href="javascript;:">
											<span class="font-twitter four-box__wrapper__social"></span>
										</a>
										<a href="javascript;:">
											<span class="font-linkedin four-box__wrapper__social"></span>
										</a>
									</div>
								</div>
							</section>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="speakers__button--wrapper padding-b">
				<a href="javascript;:" class="speakers__button color-purple relative"><span>See all Speakers&nbsp;</span><span class="font-arrow-up"></span></a>
			</div>
		</div>
		<section class="container-wrapper padding-t padding-b">
			<h2 class="section-title line-after line-after__small line-after__small__green"><?php the_field('agenda_title'); ?></h2>
			<p class="section-title__txt"><?php the_field('agenda_title_txt'); ?></p>
			<ul class="tabs tabs__day flex">
				<li class="button--green txt-white"><a href="#tab1">Wednesday, September 18</a></li>
				<li class="button--green txt-white"><a href="#tab2">Thursday, September 19</a></li>
			</ul>
			<div id="tab1" class="tab_content">
				<ul class="tabs tabs__evening">   <!-- Add tabs here -->
					<li><a href="#tab2-1">Track 1</a></li>
					<li><a href="#tab2-2">Track 2</a></li>
				</ul>
				<div id="tab2-1" class="agenda-wrapper">
					<?php if( have_rows('day_one_before_noon') ): ?>
						<?php while ( have_rows('day_one_before_noon') ) : the_row(); ?>
							<div class="flex">
								<div class="flex__time">
									<a href=""><?php the_sub_field('day_one_time_before');?></a>
								</div>
								<div class="flex__content">
									<a href=""><h5><?php the_sub_field('day_one_wrapper_title_before');?></h5></a>
									<!-- <p><?php the_sub_field('day_one_wrapper_content_before');?></p>	 -->
									<!-- <img src="<?php the_sub_field('day_one_thumbnail_before');?>" alt=""> -->
									<div class="flex">
										<h6><?php the_sub_field('day_one_speaker_name_before');?></h6>
										<p><?php the_sub_field('day_one_wrapper_interest_before');?></p>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
				<div id="tab2-2" class="agenda-wrapper">
					<?php if( have_rows('day_one_wrapper_after_noon') ): ?>
						<?php while ( have_rows('day_one_wrapper_after_noon') ) : the_row(); ?>
							<div class="flex">
								<div class="flex__time">
									<a href=""><?php the_sub_field('day_one_time_after');?></a>
								</div>
								<div class="flex__content">
									<a href=""><h5><?php the_sub_field('day_one_wrapper_title_after');?></h5></a>
									<!-- <p><?php the_sub_field('day_one_wrapper_content_after');?></p>	 -->
									<!-- <img src="<?php the_sub_field('day_one_thumbnail_after');?>" alt=""> -->
									<div class="flex">
										<h6><?php the_sub_field('day_one_speaker_name_after');?></h6>
										<p><?php the_sub_field('day_one_wrapper_interest_after');?></p>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
			<div id="tab2" class="tab_content">
				<ul class="tabs">   <!-- Add tabs here -->
					<li><a href="#tab4-1">Track 1</a></li>
					<li><a href="#tab4-2">Track 2</a></li>
				</ul>
				<div id="tab4-1" class="agenda-wrapper">
					<?php if( have_rows('day_two_before_noon') ): ?>
						<?php while ( have_rows('day_two_before_noon') ) : the_row(); ?>
							<div class="flex">
								<div class="flex__time">
									<a href=""><?php the_sub_field('day_two_time_before');?></a>
								</div>
								<div class="flex__content">
									<a href=""><h5><?php the_sub_field('day_two_wrapper_title_before');?></h5></a>
									<!-- <p><?php the_sub_field('day_two_wrapper_content_before');?></p>	 -->
									<!-- <img src="<?php the_sub_field('day_two_thumbnail_before');?>" alt=""> -->
									<div class="flex">
										<h6><?php the_sub_field('day_two_speaker_name_before');?></h6>
										<p><?php the_sub_field('day_two_wrapper_interest_before');?></p>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
				<div id="tab4-2" class="agenda-wrapper">
					<?php if( have_rows('day_two_wrapper_after_noon') ): ?>
						<?php while ( have_rows('day_two_wrapper_after_noon') ) : the_row(); ?>
							<div class="flex">
								<div class="flex__time">
									<a href=""><?php the_sub_field('day_two_time_after');?></a>
								</div>
								<div class="flex__content">
									<a href=""><h5><?php the_sub_field('day_two_wrapper_title_after');?></h5></a>
									<!-- <p><?php the_sub_field('day_two_wrapper_content_after');?></p>	 -->
									<!-- <img src="<?php the_sub_field('day_two_thumbnail_after');?>" alt=""> -->
									<div class="flex">
										<h6><?php the_sub_field('day_two_speaker_name_after');?></h6>
										<p><?php the_sub_field('day_two_wrapper_interest_after');?></p>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
		</sec>
		<div class="silver grey-light line-after line-before padding-t">
			<div class="container-wrapper">
				<h2 class="section-title price-title"><?php the_field('price_title') ;?></h2>
				<p class="section-title__txt"><?php the_field('price_title_txt'); ?></p>
				<div class="two-column-small flex">
					<?php if( have_rows('price_wrapper') ): ?>
						<?php while ( have_rows('price_wrapper') ) : the_row(); ?>
							<div class="two-column-small__content border white">
								<h4 class="two-column-small__title"><?php the_sub_field('price_wrapper_title');?></h4>
								<div class="flex">
									<div class="flex align-center">
										<span class="font-eur"></span>
										<span class="two-column-small__price cross green"><?php the_sub_field('price_wrapper_old_price');?></span>
									</div>
									<div class="flex align-center">
										<span class="font-eur"></span>
										<span class="two-column-small__price green"><?php the_sub_field('price_wrapper_new_price');?></span>
									</div>
								</div>
								<span class="two-column-small__txt"><?php the_sub_field('price_wrapper_information');?></span>
								<ul>
									<?php if( have_rows('price_information_content') ): ?>
										<?php while ( have_rows('price_information_content') ) : the_row(); ?>
											<li class="flex"><span class="font-checkmark"></span><?php the_sub_field('price_information_txt');?></li>
										<?php endwhile; ?>
									<?php endif; ?>
								</ul>
								<a href="javascript;:" class="button--green">Book Now</a>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="container-wrapper padding-t">
			<h2 class="section-title line-after line-after__small line-after__small__green"><?php the_field('sponsors_title');?></h2>
			<p class="section-title__txt"><?php the_field('sponsors_title_txt');?></p>
			<div class="flex wrap align-center">
				<?php if( have_rows('sponsors_wrapper') ): ?>
					<?php while ( have_rows('sponsors_wrapper') ) : the_row(); ?>
						<div class="four-box four-box-section sponsors">
							<a href="javascript;:">
								<img src="<?php the_sub_field('sponsors_wrapper_image');?>" alt="">
							</a>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
			<a href="javascript;:" class="button button--green button--lower button--center">Become a Sponsor</a>
		</div>
		<div class="purple">
			<div class="container-wrapper">
				<div class="flex request">
					<div>
						<p class="txt-white"><?php the_field('request_txt');?></p>
					</div>
					<div>
						<a href="javascript;:" class="button button--green"><?php the_field('request_btn');?></a>
						<?php echo do_shortcode('[contact-form-7 id="113" title="Program Request"]');?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- #content -->

	<?php get_sidebar(); ?>

</main>
<?php
get_footer();