<?php

function load_more_posts()
{
	if ( isset( $_POST['posts_offset'] ) )
	{
		$offset = $_POST['posts_offset'];
	}
	else
	{
		exit();
	}

	$url = $_SERVER['HTTP_REFERER'];
	$cat_id    = 1;

	$arg = array(
		'posts_per_page'   => 3,
		'cat'              => $cat_id,
		'offset'           => $offset,
		'suppress_filters' => false
	);

	$o = '';

	wp_reset_query();

	$query = new WP_Query( $arg );
	while ( $query->have_posts() ) : $query->the_post(); ?>

	<article id="blog-post-<?php the_ID(); ?>" class="news-box centered a-loaded hentry article-excerpt" itemscope itemtype="http://schema.org/Article">
		<figure class="article-image" style="background-image:url('<?php echo get_the_post_thumbnail_url( $post->ID, 'medium' ); ?>')">
			<meta itemprop="image" content="<?php echo get_the_post_thumbnail_url( $post->ID, 'thumbnail' ); ?>">
		</figure>
		<div class="news-box__content centered">
			<h3 itemprop="name headline">
				<?php echo the_title(); ?> </h3>
			<?php echo the_excerpt(); ?>
			<button class="btn"> Read More </button>
		</div>

		<div class="news-box-modal" data-toggle="modal">
			<figure class="article-image" style="background-image:url('<?php echo get_the_post_thumbnail_url( $post->ID, 'medium' ); ?>')">
				<meta itemprop="image" content="<?php echo get_the_post_thumbnail_url( $post->ID, 'thumbnail' ); ?>">
			</figure>
			<div class="news-box-modal__content">
				<buttton class="btn">close post</buttton>
				<div>
					<h2 itemprop="name headline">
						<?php echo the_title(); ?>
					</h2>
					<p>
						<?php  the_content(); ?>
					</p>
				</div>
			</div>
		</div>


		<span style="display:none;">
			<time itemprop="datePublished">
				<?php echo get_the_date('c'); ?>
			</time>
			<time itemprop="dateModified">
				<?php echo the_modified_date('c'); ?>
			</time>
		</span>
		<span style="display:none;" itemprop="author" itemscope itemtype="https://schema.org/Person">
			<meta itemprop="name" content="<?php echo get_author_name(); ?>">
		</span>
		<span style="display:none;" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
			<span itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
				<meta itemprop="url" content="http://www.example.com/slika.jpg">
				<meta itemprop="width" content="600">
				<meta itemprop="height" content="200">
			</span>
			<meta itemprop="name" content="Ime organizacije">
			<link itemprop="url" href="/">
		</span>

	</article>

	<?php

	endwhile;

	wp_reset_query();

}

add_action( 'wp_ajax_nopriv_get_more_posts', 'load_more_posts' );
add_action( 'wp_ajax_get_more_posts', 'load_more_posts' );