<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nex
 */

?>

<footer class="site-footer">
	<div class="container-wrapper">
		<small id="copyright">
			<div class="flex">
				<div>
					<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-2',
							'menu_id'        => 'secondary-menu',
							'menu_class'     => 'secondary-menu clear',
							'container'      => false
						) );
					?>
				</div>
				<div>
					<span class="font-twitter"></span>
					<span class="font-dribbble"></span>
					<span class="font-vimeo"></span>
					<span class="font-instagram"></span>
				</div>
			</div>
			<a href="mailto:<?php the_field('footer_contact', 'option');?>"><?php the_field('footer_contact', 'option');?></a>
			<p><?php the_field('footer_copyright', 'option');?></p>
		</small><!-- #copyright -->
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
