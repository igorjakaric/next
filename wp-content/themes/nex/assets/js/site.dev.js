jQuery(function($) {

	/*--------------------------------------------------------------
	# Components
	--------------------------------------------------------------*/
	const test = require('./components/test');
	test.init();

	/*--------------------------------------------------------------
	# Menu / Navigation
	--------------------------------------------------------------*/
	const menu = require('./components/menu');
	menu.init();

	/*--------------------------------------------------------------
	# Sliders
	--------------------------------------------------------------*/
	const sliders = require('./components/sliders');
	sliders.init();

	/*--------------------------------------------------------------
	# SmoothScroll
	--------------------------------------------------------------*/
	// const smoothScroll = require('./components/smoothScroll');
	// smoothScroll.init();

});

//-- Sticky nav : Start --//

const nav = document.querySelector('[data-main-nav]');
let topOfNav = nav.offsetTop;

function fixNav() {
	if (window.scrollY >= topOfNav) {
		document.body.style.paddingTop = nav.offsetHeight + 'px';
		document.body.classList.add('fixed-nav');
	} else {
		document.body.classList.remove('fixed-nav');
		document.body.style.paddingTop = 0;
	}
}

window.addEventListener('scroll', fixNav);

//-- Sticky nav : End --//

//-- Modals : Start --//
const buttons = document.querySelectorAll('.modal-trigger');
const modals = document.querySelectorAll('.modal');
const closeButton = document.querySelectorAll('.modal__close');

function openModal(e) {
	this.parentElement.nextElementSibling.classList.add('block');
	document.body.classList.add('modal-open')
	e.preventDefault();
}
function closeModal(e) {
	this.parentElement.parentElement.classList.remove('block');
	document.body.classList.remove('modal-open')
	e.preventDefault();
}

// window.addEventListener('keyup', (e) => {
// 	if (e.keyCode == 27) {
// 		console.log(this);
// 	}
// });

buttons.forEach( button => button.addEventListener('click', openModal));
closeButton.forEach( btn => btn.addEventListener('click', closeModal));

//-- Modals : End --//


//-- Animate on Scroll Init : Start --///
$(function() {
	AOS.init();
});
window.addEventListener('load', AOS.refresh);
//-- Animate on Scroll Init : End --///

// $('a[href*=\\#]:not(ul.nav-tabs a[href*=\\#])').on('click touchend', function(event){
// 	event.preventDefault();
// 	$('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
// });



const slideDownTriggers = document.querySelectorAll('.four-box__trigger');
const slideDownItems = document.querySelectorAll('.four-box__text');

function toggleSlide(e) {
	// this.parentElement.nextElementSibling.firstElementChild.classList.toggle('open');

	if (this.nextElementSibling.firstElementChild.classList.contains('open')) {
		this.nextElementSibling.firstElementChild.classList.remove('open');
	}

	else {
		for (let index = 0; index < slideDownItems.length; index++) {
			slideDownItems[index].classList.remove('open');
		}
		this.nextElementSibling.firstElementChild.classList.add('open');
	}
}

slideDownTriggers.forEach( slideDownTrigger => slideDownTrigger.addEventListener('click', toggleSlide));

document.querySelector('.two-column-small__content:nth-child(odd) .button').classList.add('button--green');
document.querySelector('.two-column-small__content:nth-child(even) .button').classList.add('button--purple-border');

function flip() {
    $('.card').toggleClass('flipped');
}

$( ".two-column-small__content .button" ).click(function() {
	$('.card').toggleClass('flipped');
});

enterView({

	selector: '#countUp1',
	enter: function(el) {
		const options = {
		  useEasing: true,
		  useGrouping: true,
		  separator: ',',
		  decimal: '.',
		 suffix: '+'
		};

		const CountUp1 = new CountUp('countUp1', 0, 4907, 0, 3.5, options);
		const CountUp2 = new CountUp('countUp2', 0, 100, 0, 3.5, options);
		const CountUp3 = new CountUp('countUp3', 0, 50, 0, 3.5, options);
		const CountUp4 = new CountUp('countUp4', 0, 500, 0, 3.5, options);
		const CountUp5 = new CountUp('countUp5', 0, 3, 0, 3.5, options);

	  CountUp1.start();
	  CountUp2.start();
	  CountUp3.start();
	  CountUp4.start();
	  CountUp5.start();
	},
	offset: 0.2,
	once: true,
});

enterView({
	selector: '.animation-element1',
	enter: function(el) {
		el.classList.add('in-view1');
	},
	once: true,
});

//-- Typed : Start --//
enterView({
	selector: '#typed',
	enter:function(el) {
		const typed = new Typed('#typed', {
			stringsElement: '#typed-strings',
			typeSpeed: 20,
		});
	},
	once: true,
});

enterView({
	selector: '#typed2',
	enter:function(el) {
		const typed2 = new Typed('#typed2', {
			stringsElement: '#typed-strings2',
			typeSpeed: 20,
		});

	},
	once: true,
});
//-- Typed : End --//



//-- Cookie Modal : Start --//
const cookieClose = document.querySelector('.modal--cookie__close');
const cookieBanner = document.querySelector('.modal--cookie');


const decodedCookie = decodeURIComponent(document.cookie);
const ca = decodedCookie.split(';');
for(let i=0;i<ca.length;i++){
	let spl = ca[i].split('=');
	if(spl[0] == "days"){
		cookieBanner.style.display = "none";
		break;
	}
}

function closeCookieBanner(e) {
	let d = new Date();
    d.setTime(d.getTime() + (10*24*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = "days=10;" + expires + ";path=/";

	cookieBanner.style.display = "none";
	// e.preventDefault();
}
cookieClose.addEventListener('click', closeCookieBanner)
//-- Cookie Modal : End --//

$("ul.nav-tabs a").click(function (e) {
	e.preventDefault();
	$(this).tab('show');
  });
