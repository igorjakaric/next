"use strict";

module.exports = {
  DOM: {
    slider: $('[data-slider]'),
    sliderNekiDrugi: $('.slider-neki-drugi')
  },

  init: function () {
    const DOM = this.DOM;

    // The following two slider callbacks are just examples. Feel free to edit or delete them entirely
    this.heroSlider(DOM.slider, true, 1000, false, false);
    this.defaultSlider(DOM.sliderNekiDrugi);
  },

  // The following two slider function declarations are just examples. Feel free to edit or delete them entirely
  heroSlider: function (slider, infinite, speed, arrows, dots) {
    slider.slick({
      infinite,
      speed,
      arrows,
      dots
    });
  },

  defaultSlider: function (slider) {
    slider.slick({
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      speed: 1000,
      arrows: false,
      responsive: [{
          breakpoint: 991,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2
          }
        }
      ]
    });
  }
};