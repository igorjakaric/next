"use strict";
var global = require('./global');

module.exports = {
  DOM: {
    menuBtn: $('[data-menu-btn]'),
    mainNav: $('[data-main-nav]')
  },

  init: function () {
    const DOM = this.DOM;
    const toggleHamburgerMenu = this.toggleHamburgerMenu;

    DOM.menuBtn.on('click', (e) => {
      toggleHamburgerMenu(DOM.menuBtn, DOM.mainNav, global.DOM.$body);
    });

    // Dropdown menu transition
    const $parentLink = DOM.mainNav.find('.menu-item-has-children');
    $parentLink.hover(function () {
        const $childMenu = $(this).children('.sub-menu');
        if ($childMenu.is(':visible')) {
          $childMenu.fadeOut('fast');
        } else {
          $childMenu.fadeIn('fast');
        }
      },
      function () {
        $(this).find('.sub-menu').fadeOut('fast');
      });
  },

  toggleHamburgerMenu: function (hamburgerButton, navigation, overlay) {
    hamburgerButton.toggleClass('open');
    navigation.toggleClass('slide-in-nav');
    overlay.toggleClass('overlay');
  }
};