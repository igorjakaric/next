"use strict";

module.exports = {
  init: function () {
    const scrollTime = .9;
    const scrollDistance = 300;

    $(window).on("mousewheel DOMMouseScroll", function (event) {
      event.preventDefault();

      let delta = event.originalEvent.wheelDelta / 120 || -event.originalEvent.detail / 3;
      let scrollTop = $(window).scrollTop();
      let finalScroll = scrollTop - parseInt(delta * scrollDistance);

      TweenLite.to($(window), scrollTime, {
        scrollTo: {
          y: finalScroll,
          autoKill: true
        },
        ease: Power1.easeOut,
        overwrite: 5
      });
    });
  }
};

