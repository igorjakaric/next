"use strict";

module.exports = {
  testObject: {
    condition: true
  },
  
  init: function() {
    const testObject = this.testObject;

    if (testObject.condition) {
      console.log('ES6 works!');
    }
  }
};
