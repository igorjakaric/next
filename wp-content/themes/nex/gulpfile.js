/**
 * load dependecies
 */
var gulp         = require('gulp'),
		babel 			 = require('gulp-babel'),
		browserify   = require('browserify'),
		source       = require('vinyl-source-stream'),
		buffer       = require('vinyl-buffer'),
    autoprefixer = require('autoprefixer'),
    browserSync  = require('browser-sync'),
    cleanCss     = require('gulp-clean-css'),
    concat       = require('gulp-concat'),
    concatCss    = require('gulp-concat-css'),
    filter       = require('gulp-filter'),
    flexBugsFix  = require('postcss-flexbugs-fixes'),
    ftp          = require('vinyl-ftp'),
    iconfont     = require('gulp-iconfont'),
    iconfontCss  = require('gulp-iconfont-css'),
    newer        = require('gulp-newer'),
    notify       = require('gulp-notify'),
    path         = require('path'),
    plumber      = require('gulp-plumber'),
    postcss      = require('gulp-postcss'),
    reload       = browserSync.reload,
    runSequence  = require('run-sequence'),
    sass         = require('gulp-sass'),
    sassLint     = require('gulp-sass-lint'),
    size         = require('gulp-size'),
    sourcemaps   = require('gulp-sourcemaps'),
    svgmin       = require('gulp-svgmin'),
    uglify       = require('gulp-uglify'),
    zip          = require('gulp-zip');

/**
 * Disable notifications? Uncomment this line.
 */
// process.env.DISABLE_NOTIFIER = true;

/**
 * Set paths and options
 */
var projectName = 'nex';
var popartcodeFolderName = projectName;
var themeFolderName = projectName;

/**
 * gulp deploy paths and settings
 */

var ftpHost = 'popartcode.space';
var ftpDest = '/public_html/' + popartcodeFolderName;
var ftpUser = 'popartcode';
var ftpPass = '/gdeimAmeDa!89';

var conn = ftp.create({
	host    : ftpHost,
	user    : ftpUser,
	password: ftpPass,
	parallel: 10
});

// sync-server url
var syncServer = 'http://localhost/';

// dist path
var distPath = 'dist';

// plugins paths
var pluginsCssSrc = 'assets/css/_plugins/*.css';
var pluginsJsSrc = 'assets/js/_plugins/*.js';
var pluginsDest = distPath;

// svgmin paths
var svgminSrc = 'assets/svg/*.svg';
var svgminDest = 'images/svg/';

// icon fonts paths
var iconfontSrc = svgminDest + '*.svg';
var scssFile = '../sass/base/_icon-font.scss';
var iconfontPath = 'fonts/';
var cssClass = 'font';
var iconfontName = 'fonticons';
var configFile = 'config/icon-font-config.scss';

// sass paths
var browserVersions = ['last 3 versions', 'ios >= 6'];
var sassSrc = 'sass/**/*.scss';
var sassDest = ''; // leave it empty for theme (gulpfile.js) root

// sass lint
var sassLintConfigFile = '.sass-lint.yml';

// js paths
var jsSrc = 'assets/js/site.dev.js';
var jsSrcAll = 'assets/js/**/*.js';
var jsDest = distPath;

var gulpZipWp = [
	'../../../**/*',
	'!../../../**/*.{zip,wpress,gitignore,htaccess}', // exclude
	'!../../../wp-config.php',
	'!../../../wp-content/db.php',
	'!node_modules*/',
	'!node_modules*/**/*',
	'!.idea/',
	'!.idea/**/*',
	'!zip/',
	'!zip/**/*',
	'!gulpfile.js'
];
var gulpZipTheme = [
	'**/*',
	'**/*.{zip,wpress,gitignore}',
	'!node_modules*/',
	'!node_modules*/**/*',
	'!gulpfile.js'
];

var currentdate = new Date();
var datetime = currentdate.getFullYear() + "."
	+ (currentdate.getMonth() + 1) + "."
	+ currentdate.getDate() + "-"
	+ currentdate.getHours() + "."
	+ currentdate.getMinutes() + "."
	+ currentdate.getSeconds();

/**
 * Watch with BrowserSync
 *
 * Watch for any change in files - plugins, svg fonts, sass, js, php...
 */
gulp.task('serve', function () {
	runSequence('build-dev', '_watch-sync');
});

/**
 * Watch
 *
 * Watch for any change in files - plugins, svg fonts, sass, js, php...
 */
gulp.task('watch', function () {
	runSequence('build-dev', '_watch');
});

/**
 * Build Development
 */
gulp.task('build-dev', function () {
	runSequence(['_plugins_css', '_plugins_js'], '_svgomg', '_renderfont', '_css', ['_sass-lint', '_js']);
});

/**
 * Build Distribution
 */
gulp.task('build-dist', function () {
	runSequence(['_plugins_css', '_plugins_js'], '_svgomg', '_renderfont', '_css-dist', ['_sass-lint', '_js']);
});

/**
 * Archive WordPress
 */
gulp.task('zip-wp', function () {
	return gulp.src(gulpZipWp)
	           .pipe(zip(projectName + '-wp-' + datetime + '.zip'))
	           .pipe(gulp.dest('../../../zip/'))
	           .pipe(notify(msgZip))
});

/**
 * Archive Theme
 */
gulp.task('zip-theme', function () {
	return gulp.src(gulpZipTheme)
	           .pipe(zip(projectName + '-theme-' + datetime + '.zip'))
	           .pipe(gulp.dest('../../../zip/'))
	           .pipe(notify(msgZip))
});

/**
 * Deploy Test
 */
gulp.task('test-deploy', function () {
	return gulp.src('README.md', {base: '.', buffer: false})
	           .pipe(conn.dest(ftpDest + '/wp-content/themes/' + themeFolderName))
	           .pipe(notify(msgUpload));
});

/**
 * Deploy Wordpress
 */
gulp.task('deploy-wp', function () {
	return gulp.src(gulpZipWp, {buffer: false})
	           .pipe(conn.newerOrDifferentSize(ftpDest))
	           .pipe(conn.dest(ftpDest))
	           .pipe(notify(msgUpload));
});

/**
 * Deploy Theme
 */
gulp.task('deploy-theme', function () {
	return gulp.src(gulpZipTheme, {base: '.', buffer: false})
	           .pipe(conn.newerOrDifferentSize(ftpDest + '/wp-content/themes/' + themeFolderName))
	           .pipe(conn.dest(ftpDest + '/wp-content/themes/' + themeFolderName))
	           .pipe(notify(msgUpload));
});

// sass compile
gulp.task('_css', function () {
	var prefix = [
		autoprefixer({browsers: browserVersions}),
		flexBugsFix
	];
	return gulp.src(sassSrc)
	           .pipe(plumber(msgERROR))
	           .pipe(sourcemaps.init())
	           .pipe(sass())
	           .pipe(postcss(prefix))
	           .pipe(sourcemaps.write('.'))
	           .pipe(gulp.dest(sassDest))
	           .pipe(reload({stream: true}))
	           .pipe(notify(msgSASS))
});

// sass compile distribution
gulp.task('_css-dist', function () {
	var prefix = [
		autoprefixer({browsers: browserVersions}),
		flexBugsFix
	];
	return gulp.src(sassSrc)
	           .pipe(plumber(msgERROR))
	           .pipe(sourcemaps.init())
	           .pipe(sass({outputStyle: 'compressed'}))
	           .pipe(postcss(prefix))
	           .pipe(sourcemaps.write('.'))
	           .pipe(gulp.dest(sassDest))
	           .pipe(notify(msgSASS))
});

// sass-lint
gulp.task('_sass-lint', function () {
	return gulp.src(sassSrc)
	           .pipe(sassLint({
		           config: sassLintConfigFile
	           }))
	           .pipe(sassLint.format())
	           .pipe(sassLint.failOnError())
});

// minify site.dev.js
gulp.task('_js', function () {
	return browserify(jsSrc).bundle()
	           .pipe(plumber(msgERROR))
						//  .pipe(sourcemaps.init())
						 .pipe(source('site.dev.js'))
						 .pipe(buffer())
						 .pipe(babel({presets: ['env'], compact: false}))
	           .pipe(concat('site.min.js'))
	           .pipe(uglify())
	           .pipe(sourcemaps.write('.'))
	           .pipe(gulp.dest(jsDest))
	           .pipe(notify(msgJS));
});

// watch all files with sync
gulp.task('_watch-sync', function () {
	var files = [
		'**/*.php',
		'**/*.js',
		'**/*.{png,jpg,gif,svg}'
	];

	browserSync.init(files, {
		proxy        : syncServer + projectName,
		injectChanges: true
	});

	//watch .scss files
	gulp.watch(sassSrc, ['_css', '_sass-lint']);

	//watch site.dev.js
	gulp.watch(jsSrcAll, ['_js']);

	//watch added or changed svg files to optimize them
	gulp.watch('**/*.svg', ['_watch_fonts']);

	// watch plugin files
	gulp.watch(pluginsCssSrc, ['_plugins_css']);
	gulp.watch(pluginsJsSrc, ['_plugins_js']);
});

// watch all files
gulp.task('_watch', function () {

	//watch .scss files
	gulp.watch(sassSrc, ['_css', '_sass-lint']);

	//watch site.dev.js
	gulp.watch(jsSrcAll, ['_js']);

	//watch added or changed svg files to optimize them
	gulp.watch('**/*.svg', ['_watch_fonts']);

	// watch plugin files
	gulp.watch(pluginsCssSrc, ['_plugins_css']);
	gulp.watch(pluginsJsSrc, ['_plugins_js']);
});

// concat plugin files
gulp.task('_plugins_css', function () {
	return gulp.src([pluginsCssSrc])
	           .pipe(concatCss("plugins.min.css"))
	           .pipe(cleanCss())
	           .pipe(gulp.dest(pluginsDest))
	           .pipe(notify(msgPluginsCSS))
});

gulp.task('_plugins_js', function () {
	return gulp.src([pluginsJsSrc])
	           .pipe(concat('plugins.min.js'))
	           .pipe(uglify())
	           .pipe(gulp.dest(pluginsDest))
	           .pipe(notify(msgPluginsJS))
});

// optimize svgs
gulp.task('_svgomg', function () {
	return gulp.src(svgminSrc)
	           .pipe(newer(svgminDest))
	           .pipe(svgmin({
		           plugins: [
			           {removeTitle: true},
			           {removeRasterImages: true},
			           {sortAttrs: true}
			           //{ removeStyleElement: true }
		           ]
	           }))
	           .pipe(gulp.dest(svgminDest))
});

// render fonticons
gulp.task('_renderfont', function () {
	return gulp.src(iconfontSrc)
	           .pipe(iconfontCss({
		           fontName  : iconfontName,
		           cssClass  : cssClass,
		           path      : configFile,
		           targetPath: scssFile,
		           fontPath  : iconfontPath
	           }))
	           .pipe(iconfont({
		           fontName          : iconfontName, // required
		           prependUnicode    : false, // recommended option
		           formats           : ['ttf', 'woff'], // default, 'woff2' and 'svg' are available
		           normalize         : true,
		           centerHorizontally: true
	           }))
	           .on('glyphs', function (glyphs, options) {
		           // CSS templating, e.g.
		           console.log(glyphs, options);
	           })
	           .pipe(gulp.dest(iconfontPath))
	           .pipe(notify(msgFonts))
});

// helper task for watching svgs and building fonts
gulp.task('_watch_fonts', function () {
	runSequence('_svgomg', '_renderfont', '_css');
});

/**
 * Status notifications
 */
var msgSASS = {
	title  : 'Sweet! :)',
	message: 'Styles are compiled!',
	icon   : path.join(__dirname, 'config/styles.png'),
	time   : 400,
	sound  : false,
	onLast : true
};
var msgJS = {
	title  : 'Awesome! :)',
	message: 'Script is compiled!',
	icon   : path.join(__dirname, 'config/script.png'),
	time   : 400,
	sound  : false,
	onLast : true
};
var msgPluginsJS = {
	title  : 'Wonderful! :)',
	message: 'Plugins (JS) are compiled!',
	icon   : path.join(__dirname, 'config/plugins-js.png'),
	time   : 400,
	sound  : false,
	onLast : true
};
var msgPluginsCSS = {
	title  : 'Amazing! :)',
	message: 'Plugins (CSS) are compiled!',
	icon   : path.join(__dirname, 'config/plugins-css.png'),
	time   : 400,
	sound  : false,
	onLast : true
};
var msgFonts = {
	title  : 'What a beauty! :)',
	message: 'Fonts are generated!',
	icon   : path.join(__dirname, 'config/fonts.png'),
	time   : 400,
	sound  : false,
	onLast : true
};
var msgZip = {
	title  : 'Fantastic! :)',
	message: 'Archive is zipped!',
	icon   : path.join(__dirname, 'config/zip.png'),
	time   : 400,
	sound  : false,
	onLast : true
};
var msgUpload = {
	title  : 'Excellent! :)',
	message: 'Package deployed!',
	icon   : path.join(__dirname, 'config/upload.png'),
	time   : 400,
	sound  : false,
	onLast : true
};
//error notification settings for plumber
var msgERROR = {
	errorHandler: notify.onError({
		title  : 'Fix that ERROR, bitch:',
		message: "<%= error.message %>",
		icon   : path.join(__dirname, 'config/error.png'),
		time   : 2000
	})
};